package com.bizmda.bizsip.sample.sink.api;

import com.bizmda.bizsip.common.BizException;

public interface Sink53Interface {
    public void tx01(String tranId,String data) throws BizException;
    public void compensateTx01(String tranId,String data) throws BizException;
    public void tx02(String tranId,String data) throws BizException;
    public void compensateTx02(String tranId,String data) throws BizException;
}

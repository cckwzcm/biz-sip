package com.bizmda.bizsip.sample.sink.api;

import com.bizmda.bizsip.common.BizException;

public interface Sink51Interface {
    public void tx01(String tranId,String data) throws BizException;
    public boolean queryTx01(String tranId) throws BizException;
    public void tx02(String tranId,String data) throws BizException;
    public boolean compensateTx02(String tranId,String data) throws BizException;
}

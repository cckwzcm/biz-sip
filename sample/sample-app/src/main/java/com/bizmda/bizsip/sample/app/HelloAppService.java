package com.bizmda.bizsip.sample.app;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessageInterface;
import com.bizmda.bizsip.sample.sink.api.HelloInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class HelloAppService implements AppBeanInterface {
    private HelloInterface sink22 = AppClientFactory
            .getSinkClient(HelloInterface.class,"sink22");
    private HelloInterface sink23 = AppClientFactory
            .getSinkClient(HelloInterface.class,"sink23");
    private BizMessageInterface sink24 = AppClientFactory
            .getSinkClient(BizMessageInterface.class,"sink24");
    private BizMessageInterface sink25 = AppClientFactory
            .getSinkClient(BizMessageInterface.class,"sink25");


    @Override
    public JSONObject process(JSONObject message) throws BizException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("sink22",this.sink22.hello("sink22"));
        jsonObject.set("sink23",this.sink23.hello("sink23"));
        jsonObject.set("sink24",this.sink24.call(message.set("message","sink24")).getData());
        jsonObject.set("sink25",this.sink25.call(message.set("message","sink25")).getData());
        return jsonObject;
    }
}

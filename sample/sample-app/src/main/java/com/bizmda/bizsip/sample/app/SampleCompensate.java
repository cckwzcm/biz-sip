package com.bizmda.bizsip.sample.app;

import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.sample.sink.api.Sink51Interface;
import com.bizmda.bizsip.sample.sink.api.Sink53Interface;
import com.bizmda.bizsip.sample.sink.api.Sink54Interface;

public class SampleCompensate implements SampleCompensateInterface {
    // sink51为调用第三方外部系统的sink服务
    private Sink51Interface sink51Interface = AppClientFactory
            .getSinkClient(Sink51Interface.class, "sink51");
    // sink53为内部交易处理的sink服务
    private Sink53Interface sink53Interface = AppClientFactory
            .getSinkClient(Sink53Interface.class, "sink53");
    // sink54为内部交易处理的sink服务
    private Sink54Interface sink54Interface = AppClientFactory
            .getSinkClient(Sink54Interface.class, "sink54");
    // sampleCompensate作为app服务，既能被source服务所调用，
    // 也能申明为延迟服务接口，被延迟服务所调用
    private SampleCompensateInterface delaySampleCompensateInterface
            = AppClientFactory.getDelayAppServiceClient(
                    SampleCompensateInterface.class, "sampleCompensate",
                    1000, 2000, 4000, 8000, 16000, 32000);

    @Override
    public String tx01(String tranId,String data) throws BizException {
        // 调用sink53（内部交易处理）
        this.sink53Interface.tx01(tranId,data);
        try {
            // 调用sink51（调用第三方系统）
            this.sink51Interface.tx01(tranId, data);
        } catch (BizException e) {
            if (e.isRetryDelayAppService()) {
                // 超时异常后，调用延迟服务进行向前补偿
                this.delaySampleCompensateInterface.forwardCompensateTx01(tranId, data);
                return "tx01交易向前补偿中...";
            }
            throw e;
        }
        // 成功后调用sink54（内部交易处理）
        this.sink54Interface.tx01(tranId,data);
        return "tx01交易成功";
    }

    @Override
    public void forwardCompensateTx01(String tranId,String data) throws BizException {
        // 调用sink51的查询交易状态接口（调用第三方系统），超时直接抛出BizTimeOutException异常
        boolean successFlag = this.sink51Interface.queryTx01(tranId);
        if (successFlag) {
            // 返回原调用第三方系统交易成功，继续后续调用sink54
            this.sink54Interface.tx01(tranId,data);
            return;
        } else {
            // 返回原调用第三方系统交易失败，对上步sink53的交易进行冲正
            this.sink53Interface.compensateTx01(tranId,data);
            return;
        }
    }

    @Override
    public String tx02(String tranId,String data) throws BizException {
        // 调用sink53（内部交易处理）
        this.sink53Interface.tx02(tranId, data);
        try {
            // 调用sink51（调用第三方系统）
            this.sink51Interface.tx02(tranId, data);
        } catch (BizException e) {
            if (e.isRetryDelayAppService()) {
                // 超时异常后，调用延迟服务进行向后补偿
                this.delaySampleCompensateInterface.backwardCompensateTx02(tranId, data);
                return "tx02交易向后补偿中...";
            }
            throw e;
        }
        // 成功后调用sink54（内部交易处理）
        this.sink54Interface.tx02(tranId, data);
        return "tx02交易成功";
    }

    @Override
    public void backwardCompensateTx02(String tranId,String data) throws BizException {
        // 调用sink51的冲正交易（调用第三方系统），并返回成功失败结果
        boolean successFlag = this.sink51Interface.compensateTx02(tranId, data);
        if (successFlag) {
            // 冲正交易成功，继续对上一步sink53进行冲正
            this.sink53Interface.compensateTx02(tranId, data);
            return;
        }
        else {
            // 冲正交易失败，抛出异常，提醒人工介入处理
            throw new BizException(BizResultEnum.OTHER_ERROR
                    ,"this.sink51Interface.compensateTx02()补偿失败");
        }
    }
}

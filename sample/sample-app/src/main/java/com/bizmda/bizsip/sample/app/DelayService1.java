package com.bizmda.bizsip.sample.app;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.common.BizUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DelayService1 implements AppBeanInterface {
    int retryNum = 0;

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        log.info("收到消息: {}",BizUtils.buildJsonLog(message));
        int maxRetryNum = (int)message.get("maxRetryNum");
        String result = (String) message.get("result");
        try {
            log.info("retryNum:{},休眠1000ms...",this.retryNum);
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (retryNum >= maxRetryNum) {
            this.retryNum = 0;
            if ("success".equalsIgnoreCase(result)) {
                log.info("成功返回响应!");
                return message;
            }
            else {
                log.info("返回其它异常错误!");
                throw new BizException(BizResultEnum.OTHER_ERROR);
            }
        }
        this.retryNum ++;
        log.info("返回响应超时错误,等待下一次重试...");
        throw new BizException(BizResultEnum.RETRY_DELAY_APP_SERVICE);
    }
}

package com.bizmda.bizsip.sample.sink.service;

import com.bizmda.bizsip.sample.sink.api.HelloInterface;
import com.bizmda.bizsip.sink.api.AbstractSinkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 史正烨
 */
@Slf4j
@Service
public class EchoBeanService extends AbstractSinkService implements HelloInterface {
    @Override
    public String hello(String message) {
        log.info("调用EchoBeanService.hello({})",message);
        return "hello,"+message;
    }
}

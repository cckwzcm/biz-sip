package com.bizmda.bizsip.sample.sink.controller;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.sink.api.SinkBeanInterface;
import com.bizmda.bizsip.sink.connector.ByteProcessInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 史正烨
 */
@Slf4j
@Service
public class EchoService implements ByteProcessInterface {
    @Override
    public byte[] process(byte[] inMessage) throws BizException {
        log.debug("EchoService:\n{}", BizUtils.buildHexLog(inMessage));
        return inMessage;
    }
}

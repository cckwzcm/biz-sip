package com.bizmda.bizsip.sample.sink.javaapi;

import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.sample.sink.api.CustomerDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class JavaApiBean {
    public CustomerDTO[] getCustomerList() {
        CustomerDTO[] customerDtos = new CustomerDTO[3];
        customerDtos[0] = CustomerDTO.builder()
                .customerId("001").name("张三").age(20).sex('1').build();
        customerDtos[1] = CustomerDTO.builder()
                .customerId("001").name("李四").age(30).sex('0').build();
        customerDtos[2] = CustomerDTO.builder()
                .customerId("001").name("王五").age(40).sex('1').build();
        return customerDtos;
    }

    public void saveCustomerDto(String customerId,String name) {
        log.info("saveCustomerDto({},{})",customerId,name);
    }

    public String getName(String id) {
        log.info("getName({})",id);
        return "张三";
    }

    public int getAge(String id) {
        log.info("getAge({})",id);
        return 30;
    }
}

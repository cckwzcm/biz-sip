package com.bizmda.bizsip.sample.sink.controller;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.sink.api.SinkBeanInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 史正烨
 */
@Slf4j
@Service
public class CrmServer implements SinkBeanInterface {
    private static final Map<String,String> ID_NAME_MAP = new HashMap<>();
    static {
        ID_NAME_MAP.put("003","张三");
        ID_NAME_MAP.put("004","李四");
        ID_NAME_MAP.put("005","王五");
    }

    @Override
    public JSONObject process(JSONObject jsonObject) throws BizException {
        String accountNo = (String)jsonObject.get("accountNo");
        String accountName = ID_NAME_MAP.get(accountNo);
        if (accountName == null) {
            throw new BizException(100,"账户不存在!");
        }
        jsonObject.set("accountName",accountName);
        return jsonObject;
    }
}

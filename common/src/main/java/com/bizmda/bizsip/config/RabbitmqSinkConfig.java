package com.bizmda.bizsip.config;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import org.springframework.context.annotation.Bean;

import java.util.Map;

/**
 * @author 史正烨
 */
@Getter
public class RabbitmqSinkConfig extends AbstractSinkConfig {
    private String routingKey;
    private String exchange;
    private String queue;

    public RabbitmqSinkConfig(Map map) {
        super(map);
        this.routingKey = (String) map.get("routing-key");
        if (StrUtil.isEmpty(this.routingKey)) {
            this.routingKey = this.getId();
        }
        this.exchange = (String) map.get("exchange");
        if (StrUtil.isEmpty(this.exchange)) {
            this.exchange = "exchange.dircect.bizsip.sink";
        }
        this.queue = (String) map.get("queue");
        if (StrUtil.isEmpty(this.queue)) {
            this.queue = "queue.bizsip.sink."+this.getId();
        }
    }

    @Override
    public String toString() {
        String str = super.toString()
                + StrUtil.format(",exchange={},route-key={},queue={}",
        this.exchange,this.routingKey,this.queue);
        return str;
    }
}

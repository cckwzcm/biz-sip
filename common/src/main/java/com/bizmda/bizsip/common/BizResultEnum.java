package com.bizmda.bizsip.common;

/**
 * @author 史正烨
 */

public enum BizResultEnum implements ResultEnumInterface {

    // App层错误（1-99）
    APP_SERVICE_NOT_FOUND(1,"聚合服务不存在"),
    APP_SERVICE_CLASS_LOAD_ERROR(2,"聚合服务实现类装载失败"),
    ASYNC_APP_SERVICE_PARENT_TRANCTION_BINDDING_ERROR(4,"异步回调服务父服务绑定失败"),
    ASYNC_APP_SERVICE_CONTEXT_NOT_FOUND(5,"异步服务上下文不存在"),
    INTEGRATOR_SCRIPT_RETURN_EXECUTOR_ERROR(7,"整合器返回ExecutorError错误类"),
    RETRY_DELAY_APP_SERVICE(8,"重试App延迟服务"),
    APP_SERVICE_MAXIMUM_RETRY_ERROR(9,"App服务重试次数超限"),
    APP_SERVICE_METHOD_NOT_FOUND(10,"App服务的Method方法不存在"),

    // source层错误（100-199）
    SOURCE_RETURN_NULL(101,"服务整合器返回为空"),
    SOURCE_ID_NOTFOUND(102,"Source服务接入端ID不存在"),
    // sink层错误（200-299）
    SINK_FILE_NOTFOUND(201,"sink.yml文件不存在"),
    SINK_SINKID_IS_NULL(202,"sinkId为空"),
    SINK_NOT_SET(203,"对应sink没有设置"),
    SINK_TYPE_IS_ERROR(204,"sink类型设置出错"),
    SINK_JSON_BEAN_SINK_INTERFACE_ERROR(205,"类要求是JSON的sink-bean接口类型"),
    SINK_CLASSNAME_NOT_SET(206,"Sink对应类外没有设置"),
    SINK_MQ_TIMEOUT(207,"消息服务超时无返回"),
    // 格式转换器错误（300-399）
    CONVERTOR_NOT_SET(301,"没有设置消息转换处理器"),
    CONVERTOR_NOTFOUND_MATCH_RULE(302,"没有找到匹配的消息断言规则"),
    CONVERTOR_NO_CONFIG_FILE(303,"对应的消息转换配置文件不存在"),
    CONVERTOR_CREATE_ERROR(304,"消息处理器Java类创建失败"),
    CONVERTOR_NOTFOUND_FIELD_FUNCTION(305,"域处理函数没有找到实现方法"),
    CHECKRULE_FIELD_FUNCTION_EXECUTE_ERROR(306,"域处理函数执行出错"),
    CHECKRULE_FIELD_CHECK_ERROR(307,"域校验出错"),
    CHECKRULE_FIELD_CHECK_FUNCTION_NOTFOUND(308,"域校验函数不存在"),
    CHECKRULE_FIELD_CHECK_THREAD_ERROR(309,"域校验函数线程执行错误"),
    CHECKRULE_SERVICE_CHECK_THREAD_ERROR(310,"服务校验线程执行错误"),
    CHECKRULE_FILE_NOTFOUND(311,"检验规划文件不存在"),
    CHECKRULE_SERVICE_CHECK_ERROR(312,"服务规则校验出错"),
    CONVERTOR_ISO8583_COFIG_ERROR(313,"ISO8583报文定义错误"),
    CONVERTOR_UNSUPPORTED_ENCODE_ERROR(314,"转换码制出错"),
    CONVERTOR_ISO8583_PACK_ERROR(315,"ISO8583打包错误"),
    // 通讯适配器错误（400-499）
    CONNECTOR_TCP_CONNECT_ERROR(401,"无法连接TCP服务端口"),
    CONNECTOR_TCP_READ_TYPE_ERROR(402,"未知的channelRead读取类型"),
    CONNECTOR_JAVA_CLASS_CREATE_ERROR(403,"connector创建Java类失败"),
    CONNECTOR_IN_PARAMETER_TYPE_ERROR(404,"传入参数类型和适配器类型不匹配"),
    CONNECTOR_NOT_SET(407,"没有设置Connector"),
    CONNECTOR_HTTP_SINK_TYPE_ERROR(408,"Http Sink传入参数type类型错误!"),
    CONNECTOR_NO_BYTE_PROCESS_INTERFACE(409,"connector没有实现ByteProcessInterface接口"),
    // 其它错误（500-999以上）
    OTHER_EL_EXECUTE_ERROR(501,"EL表达式计算出错"),
    OTHER_NO_MATCH_SERVICE_RULE(502,"没有找到匹配的断言规则"),
    OTHER_UNSUPPORTED_ENCODING_ERROR(503,"不支持的编码方式"),
    OTHER_JAVA_API_PARAMETER_ERROR(504,"Java API输入参数错误"),
    OTHER_JAVA_CLASS_METHOD_ERROR(505,"执行Java方法失败"),
    OTHER_FILE_NOTFOUND(506,"文件不存在"),
    OTHER_CLASS_NOTFOUND(507,"Java类不存在"),
    OTHER_ERROR(599,"其它错误"),
    SUCCESS(0,"成功");

    private int code;
    private String message;

    BizResultEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}

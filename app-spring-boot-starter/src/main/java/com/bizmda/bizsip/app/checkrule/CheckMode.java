package com.bizmda.bizsip.app.checkrule;

public enum  CheckMode {
    // 检查出一个有问题即返回
    ONE,
    // 全部检查，返回全部错误
    ALL
}

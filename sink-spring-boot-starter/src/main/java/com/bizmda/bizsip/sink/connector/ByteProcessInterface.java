package com.bizmda.bizsip.sink.connector;

import com.bizmda.bizsip.common.BizException;

/**
 * @author shizhengye
 */
public interface ByteProcessInterface {
    /**
     * service类型Connector类，要实现的基于字节流byte[]作为出入参数的接口
     * @param inMessage
     * @return
     * @throws BizException
     */
    byte[] process(byte[] inMessage) throws BizException;
}

package com.bizmda.bizsip.sink.connector;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.sink.api.SinkBeanInterface;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 史正烨
 */
@Slf4j
@Getter
public class ServiceSinkConnector extends AbstractSinkConnector implements ByteProcessInterface {
    private ByteProcessInterface sinkBean;
    private String clazzName;

    @Override
    public void init(AbstractSinkConfig sinkConfig) throws BizException {
        super.init(sinkConfig);
        this.clazzName = (String) sinkConfig.getConnectorMap().get("class-name");
        log.info("初始化ServiceSinkConnector: class-name={}",this.clazzName);
        try {
            Object sinkBean = SpringUtil.getBean(Class.forName(this.clazzName));
            if (!(sinkBean instanceof ByteProcessInterface)) {
                throw new BizException(BizResultEnum.CONNECTOR_NO_BYTE_PROCESS_INTERFACE);
            }
            this.sinkBean = (ByteProcessInterface)sinkBean;
        } catch (ClassNotFoundException e) {
            log.error("获取类出错",e);
            throw new BizException(BizResultEnum.CONNECTOR_JAVA_CLASS_CREATE_ERROR,e);
        }
    }

    @Override
    public byte[] process(byte[] inMessage) throws BizException {
        log.debug("调用ServiceSinkConnector: class-name={}",this.clazzName);
        return this.sinkBean.process(inMessage);
   }
}
